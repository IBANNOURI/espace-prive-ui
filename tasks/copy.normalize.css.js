// Config
var config = require('../config.json');

// Require
var gulp = require('gulp');
var gutil = require('gulp-util');
var gulpCopy = require('gulp-copy');
var plumber = require('gulp-plumber');

// copy bower_components:normalize-css
gulp.task('copy:normalize:css', function() {
    return gulp.src(config.bower.normalize)
        .pipe(gulpCopy(config.styles.vendor_build, {prefix: 4}));
});
