// Config
var config = require('../config.json');

// Require
var gulp = require('gulp');
var gutil = require('gulp-util');
var gulpCopy = require('gulp-copy');
var plumber = require('gulp-plumber');

// copy bower_components:foundation
gulp.task('copy:foundation:scss', function() {
    return gulp.src(config.bower.foundation_styles)
        .pipe(gulpCopy(config.styles.vendor_libs + '/foundation', {prefix: 3}));
});

// copy bower_components:foundation:vendor
gulp.task('copy:foundation:vendor:scss', function() {
    return gulp.src(config.bower.foundation_vendor)
        .pipe(gulpCopy(config.styles.vendor_libs + '/_vendor', {prefix: 3}));
});