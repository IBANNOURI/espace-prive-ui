// Config
var config = require('../config.json');
var pcavendor = require('../mlnetvendor.json');

// Require
var gulp = require('gulp');
var gutil = require('gulp-util');
var plumber = require('gulp-plumber');
var inquirer = require('inquirer');
var del = require('del');
var runSequence = require('run-sequence');
var chalk = require('chalk');
var _ = require('lodash');
var fs = require('fs-extra')

var templateChalkGreen = chalk.bold.green;
var templateChalkRed = chalk.bold.red;

gulp.task('copy:vendor', function(callback) {
    var labels = [];

    _.forEach(pcavendor, function(value, key) {
        var label = {};
        label['name'] = key;
        label['checked'] = value.checked;

        labels.push(label);
    });

    var options = {
        type: 'checkbox',
        name: 'vendorList',
        message: 'Choose dependencies to install :',
        choices: labels,
        pageSize: labels.length,
    };

    inquirer.prompt(options).then(function (answers) {
        var vendors = answers.vendorList;
        var vendorsCount = 0;

        _.forEach(vendors, function(value, key) {
            var vendor = pcavendor[value];

            if(vendor.jsPath.length)
            {
                pcaFileMove(vendor.jsPath, 'js');
            }

            if(vendor.cssPath.length)
            {
                pcaFileMove(vendor.cssPath, 'css');
            }

            if(vendorsCount == vendors.length - 1)
            {
                callback();
            }

            vendorsCount ++;
        });
    });
});

function pcaFileMove(paths, type) {
    var assetsPath = '';

    switch(type)
    {
        case 'js' :
            assetsPath = config.scripts.vendor;
            break;
        case 'css' :
            assetsPath = config.styles.vendor_build;
            break;
        default :
            console.log('Nothing todo here');
    }

    _.forEach(paths, function(value) {
        var fileName = value.substr(value.lastIndexOf('/') + 1);
        var src = config.bower.libs + '/' + value;
        var dest = assetsPath + '/' + fileName;

        fs.copy(src, dest, function(err) {
            if(err)
            {
                console.log(err);
            }
        });
    });
};