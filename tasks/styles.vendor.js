// Config
var config = require('../config.json');

// Require
var gulp = require('gulp');
var gutil = require('gulp-util');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var prompt = require('gulp-prompt');
var del = require('del');
var runSequence = require('run-sequence');
var chalk = require('chalk');

var templateChalkGreen = chalk.bold.green;

// scss to css [vendor:foundation]
gulp.task('styles:vendor:foundation', function(callback) {
    gulp.src('tasks/styles.vendor.js').pipe(prompt.prompt({
        type: 'list',
        name: 'foundationInstallType',
        message: 'With what type of Foundation\'s intall would you like to proceed ?',
        choices: ['gridOnly', 'full']
    }, function(res){
        var res = res.foundationInstallType;

        switch(res)
        {
            case 'gridOnly':
            default:
                runSequence('styles:vendor:foundation:grid', callback);
                break;

            case 'full':
                runSequence('styles:vendor:foundation:full', callback);
                break;
        };
    }));
});

gulp.task('styles:vendor:foundation:grid', function() {
    pathScss = '/export/foundation--grid.scss';

    console.log(templateChalkGreen('/-------------------------------------------------------/'));
    console.log(templateChalkGreen('/ You choose to import only the flex grid, please wait. /'));
    console.log(templateChalkGreen('/-------------------------------------------------------/'));

    return gulp.src(config.styles.vendor_libs + pathScss)
        .pipe(plumber())
        .pipe(sass({
            //outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(gulp.dest(config.styles.vendor_build));
});

gulp.task('styles:vendor:foundation:full', function() {
    pathScss = '/export/foundation--full.scss';

    console.log(templateChalkGreen('/-----------------------------------------------------/'));
    console.log(templateChalkGreen('/ You choose to import the full package, please wait. /'));
    console.log(templateChalkGreen('/-----------------------------------------------------/'));

    return gulp.src(config.styles.vendor_libs + pathScss)
        .pipe(plumber())
        .pipe(sass({
            //outputStyle: 'compressed'
        }).on('error', sass.logError))
        .pipe(gulp.dest(config.styles.vendor_build));
});
