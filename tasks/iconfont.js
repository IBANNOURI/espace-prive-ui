// Config
var config = require('../config.json');

// Require
var gulp = require('gulp');
var gutil = require('gulp-util');
var plumber = require('gulp-plumber');
var iconfont = require('gulp-iconfont');
var del = require('del');
var consolidate = require('gulp-consolidate');
var browserSync = require('browser-sync').create();
var runTimestamp = Math.round(Date.now()/1000);
var fs = require('fs');


// iconFont
gulp.task('iconfont', function(){
    del(config.fonticon.build);
    del(config.styles.vendor_build + '/fonticon.css');

    return gulp.src([config.fonticon.src + '*.svg'])
        .pipe(plumber())
        .pipe(iconfont({
            fontName: 'iconfont', // required
            appendUnicode: true, // recommended option
            formats: ['ttf', 'eot', 'woff'], // default, 'woff2' and 'svg' are available
            timestamp: runTimestamp, // recommended to get consistent builds when watching files

            normalize: true,
        }))
        .on('glyphs', function(glyphs, options) {
            // CSS templating, e.g.
            gulp.src(config.fonticon.tpl + 'fonticon.css')
                .pipe(consolidate('lodash', {
                    glyphs: glyphs,
                    fontName: 'iconfont',
                    fontPath: config.fonticon.dist,
                    className: 'c-fonticon',
                }))
                .pipe(gulp.dest(config.styles.vendor_build));
        })
        .pipe(gulp.dest(config.fonticon.build))
        .pipe(browserSync.stream());
});

// generate MLNET_common fonticon
gulp.task('fonticon:common', function(){
    del(config.fonticon.build_common);
    del(config.styles.vendor_build + '/mlnet__common.css');

    return gulp.src([config.fonticon.src_common])
        .pipe(plumber())
        .pipe(iconfont({
            fontName: 'mlnet__common', // required
            appendUnicode: true, // recommended option
            formats: ['ttf', 'eot', 'woff'], // default, 'woff2' and 'svg' are available
            normalize: true,
            fontHeight: 1001, // Tried lot of values, <1000 and also 10000, and 100000 :P but no success
            centerHorizontally: true,
        })) .on('glyphs', function(glyphs, options) {
            // CSS templating, e.g.
           // console.log(glyphs);
            fs.writeFileSync('build/build-date.json',JSON.stringify(glyphs));
        })
        .on('glyphs', function(glyphs, options) {
            // CSS templating
            gulp.src(config.fonticon.tpl_common)
                .pipe(plumber())
                .pipe(consolidate('lodash', {
                    glyphs: glyphs,
                    fontName: 'mlnet__common',
                    fontPath: config.fonticon.dist_common,
                    className: 'c-fmlnet',
                    centerHorizontally: true,
                }))
                .pipe(gulp.dest(config.styles.vendor_build));

            gulp.src("app/libs/tpl/mlnet__common_react.css")
                .pipe(plumber())
                .pipe(consolidate('lodash', {
                    glyphs: glyphs,
                    fontName: 'mlnet__common',
                    fontPath: config.fonticon.dist_common_react,
                    className: 'c-fmlnet',
                    centerHorizontally: true,
                }))
                .pipe(gulp.dest(config.styles.vendor_build));

        })
        .pipe(gulp.dest(config.fonticon.build_common))
        .pipe(gulp.dest("../marocleasing/src/core/assets/fonts/mlnet_common"));
});
