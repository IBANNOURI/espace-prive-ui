// Config
var config = require('../config.json');

// Require
var gulp = require('gulp');
var data = require('gulp-data');
var fs = require('file-system');
var gutil = require('gulp-util');
var plumber = require('gulp-plumber');
var del = require('del');
var pugjs = require('gulp-pug');
var browserSync = require('browser-sync').create();


// pugjs to html
gulp.task('views:pug', function() {
    //del([config.views.build + '/*.html', '!' + config.views.build + '/index.html']);

    return gulp.src(config.views.src)
        .pipe(plumber())
        .pipe(data( function(file) {
            return JSON.parse(
                fs.readFileSync('app/views/data/data-settings.json')
            );
        } ))
        .pipe(pugjs({
            pretty: true
        }))

        .pipe(gulp.dest(config.views.build))
        .pipe(browserSync.stream());
});


gulp.task('templates', function() {
    return gulp.src('app/views/*.pug')
        .pipe(data( function(file) {
            return JSON.parse(
                fs.readFileSync('app/views/data/data-settings.json')
            );
        } ))
        .pipe(pugjs())
        .pipe(gulp.dest('build'))``
});

gulp.task('templatespug', function() {
    return gulp.src('./app/views/*.pug')
        .pipe(data(function(file) {
            return { "data": require('app/views/data/data-settings.json') }
        }))
        .pipe(pugjs({
            pretty: true
        }))
        .pipe(gulp.dest('build'))
});