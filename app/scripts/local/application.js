var $document = jQuery(document),
    $window = jQuery(window),
    $htmlBody = jQuery('html, body'),

    $body = jQuery('body');
// Global namespace
var application = application || {};
application.layouts = application.layouts || {};
application.dataTables = application.dataTables || {};
application.login = application.login || {};
application.mlnetforms = application.mlnetforms || {};
application.swalModals = application.swalModals || {};
application.alert = application.alert || {};
application.simulation = application.simulation || {};


application.layouts = {
    init : function(){
        mlnetNav.init();
        mlnetGoToTop.init();
        jQuery(".mlnet-simulation_tab").find("input:radio").change(function () {
            //alert("test")
            jQuery(".panel-footer__actions").find(".button").removeClass("disabled");
        });
        jQuery(document).on("change", ".file-upload", function(){
            var filepath = this.value;
            var m = filepath.match(/([^\/\\]+)$/);
            var filename = m[1];
            jQuery(this).parent().prev("input:text").val(filename);
        });
        this.magnificPopupInit();

        jQuery(document).on("keyup", ".js-charCount", function(){
            var elt = jQuery(this);
            var max = elt.attr("data-count");
            var len = elt.val().length;
            if (len >= max) {
                jQuery('#charCountNum').text('Vous avez atteint la limite');
            } else {
                var char = max - len;
                jQuery('#charCountNum').text(char + ' caractères restants');
            }
        });
        jQuery(document).on("click", ".js-cta-add_fileUpload", function(){
            var elt = jQuery(this);
            var tpl = jQuery("#fileUploadTemplate").html();
            var container = jQuery(".js-fileUpload_container");
            container.append(tpl);
            return false;

        });
        jQuery(document).on("click", ".j-cta-tab", function(){
            jQuery(".j-cta-tab").removeClass("clear");
            jQuery(this).addClass("clear");
            var id = jQuery(this).attr("data-targettab")

            jQuery(".proposal-tab_content").hide();
            jQuery("#"+id).show();

            return false;

        });
        jQuery(document).on("click", ".js-fileUpload-line button", function(){
           jQuery(this).parents(".js-fileUpload-line").remove();
            return false;

        });




    },
    magnificPopupInit : function () {
       /* if(jQuery('.js-open-inpop').length){
            jQuery('.js-open-inpop').each(function (i,item) {
                jQuery(item).magnificPopup({
                    type: 'inline',
                    showCloseBtn: false,
                    items: {src : jQuery(jQuery(item).attr('data-target'))},
                });
            })
        }*/
        jQuery(document).on("click", ".modal__close, .button.alert", function(){
            jQuery.magnificPopup.close();
        })
        jQuery(document).on("click", ".js-open-inpop", function(){
            var target = jQuery(jQuery(this).attr('data-target'))
            jQuery.magnificPopup.open({
                type: 'inline',
                showCloseBtn: false,
                items: {src : target }
            });
            return false;
        })
    }
};

application.dataTables = {
    init : function(){
      mlnetDatatables.init();
    },
};


application.login = {
    init : function(){},
};



application.mlnetforms = {
    init : function(){
        mlnetForm.init();
    }
};


application.swalModals = {
    init : function(){
        swalModal.init();
    }
};

application.alert = {
    init : function(){
        mlnetAlert.init();
    }
};


application.simulation = {
    init : function(){
       // mlnetSimulation.init();
    }
};

