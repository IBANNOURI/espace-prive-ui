var mlnetAlert;

mlnetAlert = {
    init: function() {
        if(jQuery('[class*="ml-alert"]').length){
            jQuery('[class*="ml-alert"]').each(function(index, item){
                if(jQuery(item).attr("data-delay") && jQuery(item).attr("data-delay") != "")
                    jQuery(item).delay(parseFloat(jQuery(item).attr("data-delay"))).slideUp("fast",function() {
                        jQuery(this).remove();
                    });
            })
        }
        this._eventHandler();
    },

    _eventHandler: function() {
        var self = this;

        jQuery('body').on('click', '[class*="ml-alert"] .j-close_alert', function(ev) {
            ev.preventDefault();
            self._removeItem(jQuery(this));
        });
    },

    _removeItem: function($el) {
        var $alert = $el.parents('[class*="ml-alert"]');
        $alert.slideUp(function() {
            $alert.remove();
        });
    }
};