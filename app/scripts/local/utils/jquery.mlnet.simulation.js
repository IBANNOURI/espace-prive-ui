var mlnetSimulation = mlnetSimulation || {};
mlnetSimulation = {
    init: function () {
        this._eventHandler();
        this._initialState();
    }
    _eventHandler : function(){

    }
    _initialState : function(){

    },

    noUiSliderInit : function () {
        if (!$(".double-handle-slider").length) return false;

        var doubleHandleSlider = document.querySelector('.double-handle-slider');
        var minValInput = document.querySelector('.min-value');
        var maxValInput = document.querySelector('.max-value');


        noUiSlider.create(doubleHandleSlider, {
            start: [5000, 100000],
            connect: !0,
            range: {
                min: 5000,
                max: 500000
            },
            prefix: "MAD",
            format: {
                from: function (value) {
                    return parseInt(value);
                },
                to: function (value) {
                    return parseInt(value);
                }
            }
        }).on("update.ccProjectSlider", function (t, e) {
            $(".slider-title span.min").text(t[0])
            $(".slider-title span.max").text(t[1])
            $("#min-price").val(t[0])
            $("#max-price").val(t[1])

        });
    }

}


//range 1 script
   var slider = document.getElementById("myRange");
   var taux;
var output = document.getElementById("dureei");
output.innerHTML = slider.value;
 document.getElementById('dureei').addEventListener('change', change, false);
 function change(e){
     document.getElementById("myRange").value = document.getElementById("dureei").value;
 }

slider.oninput = function() {
  document.getElementById("dureei").value = this.value;
}
//range 2 script
var slider1 = document.getElementById("myRange1");
var output1 = document.getElementById("dureem");
output.innerHTML = slider1.value;
 document.getElementById('dureem').addEventListener('change', change1, false);
 function change1(e){
     document.getElementById("myRange1").value = document.getElementById("dureem").value;
 }

slider1.oninput = function() {
  document.getElementById("dureem").value = this.value;
}
//fin ranges script
    var opt=document.getElementById("options");
    var step1 = document.getElementById("1");
    var step2 = document.getElementById("2")
    var step3 = document.getElementById("3");
    var step4 = document.getElementById("4");
    var bars = document.getElementsByClassName("empty_bar");
    var texts =document.getElementsByClassName("txtprog");

    //  var nom=document.getElementById("nom_mobilier").value;
    //  var prenom=document.getElementById("prenom_mobilier").value;
    //  var cin=document.getElementById("cin_mobiler").value;
    //  var raison_soc=document.getElementById("r_social").value;
    //  var fixe=document.getElementById("tele_mobilier").value;
    //  var gsm=document.getElementById("gsm_mobilier").value;
    //  var email=document.getElementById("email_mobilier").value;
    //  var rc=document.getElementById("rc_mobilier").value;
    //  var v_list=document.getElementById("ville_rc");
    //  var ville_in=v_list.options[v_list.selectedIndex].value;
    //  var c_terms=document.getElementById("terms").checked;
    //  var data_use=document.getElementById("data_usage").checked;

       function suivid(){
        //   alert("suivie de demande");
        opt.value ="S";
        //   document.getElementById("content2").style.display="block";
        //   document.getElementById("content1").style.display="none";
       // alert("pas encore implementer!");

       }
       function nouvelled(){

    document.getElementsByClassName("entry-content")[0].style.background = "#eaeaea";
           opt.value="N";
        //   alert("Nouvelle demande");
        if(window.innerWidth>768){
            document.getElementById("content2").style.display="-webkit-inline-box";
           document.getElementById("content1").style.display="none";
        }else{
             document.getElementById("content2").style.display="block";
           document.getElementById("content1").style.display="none";
        }

       }
       function pliberale(){
           document.getElementById("content4").style.display="block";
           document.getElementById("form_inscription").style.display="block";
           document.getElementById("content1").style.display="none";
            document.getElementById("content2").style.setProperty("display", "none", "important")
        //   opt.value +="L";
         if(opt.value.substr(1,1)==''){
            opt.value +="L";
        }
        else
            opt.value=opt.value.replace("E","L");
            fill_cities("ville_rc");
            fill_options();
        //   alert(opt.value);
       }
       function entreprise(){

           document.getElementById("content4").style.display="block";
            document.getElementById("form_inscription").style.display="block";
           document.getElementById("content1").style.display="none";
            document.getElementById("content2").style.display="none";
        //   opt.value +="E";
        if(opt.value.substr(1,1)==''){
            opt.value +="E";
        }
        else{
            opt.value=opt.value.replace("L","E");
        }



            fill_cities("ville_rc");
            fill_options();
       }
       function new_demhover(){
           document.getElementById("new_demt").style.color="#ec9102";
          document.getElementById("new_dem").src="../traitement_inscription/UI/Shape3.png";
       }
       function new_demout(){
           document.getElementById("new_demt").style.color="white";
            document.getElementById("new_dem").src="../traitement_inscription/UI/Shape3.png";
       }
       function suiv_demhover(){
           document.getElementById("suiv_demt").style.color="#ec9102";
           document.getElementById("suiv_dem").src="../traitement_inscription/UI/LastVersion/suivi.png";
       }
       function suiv_demout(){
           document.getElementById("suiv_demt").style.color="white";
           document.getElementById("suiv_dem").src="../traitement_inscription/UI/LastVersion/suivi.png";
       }
       function proflibhover(){
           document.getElementById("proflibt").style.color="#ec9102";
           document.getElementById("proflib").src="../traitement_inscription/UI/profession_liberele_orange_v2.png";
       }
       function proflibout(){
           document.getElementById("proflibt").style.color="#7f8082";
           document.getElementById("proflib").src="../traitement_inscription/UI/profession_liberele_v2.png";
       }
       function entrehover(){
           document.getElementById("entret").style.color="#ec9102";
           document.getElementById("entre").src="../traitement_inscription/UI/professionnel_v3.png";
       }
       function entreout(){
           document.getElementById("entret").style.color="#7f8082";
           document.getElementById("entre").src="../traitement_inscription/UI/professionnel_gris_v3.png";
       }
       function cmobilier(){
           document.getElementById("credit_mob").style.display="block";
            opt.value+="M";

          document.getElementById("content1").style.display="none";
            document.getElementById("content2").style.display="none";


        fill_options();
       }
       function set_type(){
            document.getElementById("div_demande_simu").style.display="block";
           var e = document.getElementById("m_type");

     var type = e.options[e.selectedIndex].value;
    //  if(opt.value.length >= 3){
    //      opt.value=opt.value.substr(0,2);
    //      opt.value+=type;
    //  }
    //  else{
    //   opt.value+=type;
    //  }
    //   alert(opt.value);
    if(opt.value.substr(2,1)==''){
        opt.value+=type;
        // alert(opt.value);
    }
    else{
        opt.value=opt.value.replace(opt.value.substr(2,1),type);
        // alert(opt.value);
    }
       if(opt.value.substr(2,1) == '0' || opt.value.substr(2,1)== '1'){

           document.getElementById("form_simulation_mobilier").style.display="block";
           document.getElementById("contrat3").style.display = "none";
           document.getElementById("contrat").style.display= "block";
           document.getElementById("form_simulation_immobilier").style.display="none";

       }
       else{
           document.getElementById("form_simulation_immobilier").style.display="block";
           document.getElementById("contrat3").style.display = "block";
           document.getElementById("contrat").style.display= "none";
              document.getElementById("form_simulation_mobilier").style.display="none";

       }


            // document.getElementById("content3").style.display="none";
            //  document.getElementById("content4").style.display="block";
            //  alert(opt.value);
            // alert(type);
              set_type_bien();
              document.getElementById("type_de_biens").style.display="block";

       }
       function creimmobilier(){

           opt.value +="In";
        //   alert(opt.value);
             document.getElementById("content4").style.display="block";


           document.getElementById("content1").style.display="none";
            document.getElementById("content2").style.display="none";
             document.getElementById("content3").style.display="none";
             fill_options();


       }
       function fill_options(){
           var type_client=opt.value.substr(1,1);
           switch(type_client){
               case 'E' : document.getElementById("entreprise").style.display="block";document.getElementById("particulier").style.display="none";break;
               case 'L' : document.getElementById("particulier").style.display="block";document.getElementById("entreprise").style.display="none";break;
           }

       }

       function next_step(){
        //   alert(opt.value);
        //   var type_credit=opt.value.substr(2,1);
                var nom=document.getElementById("nom_mobilier").value;
              var prenom=document.getElementById("prenom_mobilier").value;
              var cin=document.getElementById("cin_mobiler").value;
              var raison_soc=document.getElementById("r_social").value;
              var fixe=document.getElementById("tele_mobilier").value;
              var gsm=document.getElementById("gsm_mobilier").value;
              var email=document.getElementById("email_mobilier").value;
              var rc=document.getElementById("rc_mobilier").value;
              var v_list=document.getElementById("ville_rc");
              var ville_in=v_list.options[v_list.selectedIndex].value;
              var c_terms=document.getElementById("terms").checked;
              var data_use=document.getElementById("data_usage").checked;
           if(opt.value.substr(1,1)=='L'){

               if(nom =='' || gsm =='' || email =='' || c_terms==false){
                   alert(" Vous devez au moins remplir Les champs : Nom ,Telephone mobile,E-MAIL et accepter les conditions d'utilisation avant de poursuivre votre demande");
               }
               else{
                     var xmlhttp= window.XMLHttpRequest ?
 new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

 xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
    {
        if(xmlhttp.responseText=='done'){
                 document.getElementById("simulation").style.display="block";
           document.getElementById("form_inscription").style.display="none";
           texts[1].style.color="#ec9102";
           bars[0].src="../traitement_inscription/UI/progress_bar/bar_orange.png";
           bars[1].src="../traitement_inscription/UI/progress_bar/bar_half_orange.png";
           bars[2].src="../traitement_inscription/UI/progress_bar/bar.png";
           step2.src="../traitement_inscription/UI/progress_bar/2_orange.png";
           step3.src="../traitement_inscription/UI/progress_bar/3.png";
           step4.src="../traitement_inscription/UI/progress_bar/4.png";
        }
        else
        alert(xmlhttp.responseText);

        }

    }
     xmlhttp.open("POST","../traitement_inscription/etape1.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("nom=" + nom +"&prenom=" + prenom+"&cin="+cin+"&tel_fixe=" +fixe+"&gsm="+gsm+"&email="+email+"&rc="+rc+"&ville="+ ville_in + "&cndp="+c_terms+"&prsp="+data_use+"&type_ent="+opt.value.substr(1,1));

    }




           }
           else{

               if(raison_soc =='' || gsm=='' || email=='' || c_terms==false){
                   alert("Vous devez au moins remplir Les champs : raison sociale ,Telephone mobile,E-MAIL et accepter les conditions d'utilisation avant de poursuivre votre demande");

               }
               else{
            var xmlhttp= window.XMLHttpRequest ?
 new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

 xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
    {
        if(xmlhttp.responseText=='done'){
                 document.getElementById("simulation").style.display="block";
           document.getElementById("form_inscription").style.display="none";
           texts[1].style.color="#ec9102";
           bars[0].src="../traitement_inscription/UI/progress_bar/bar_orange.png";
           bars[1].src="../traitement_inscription/UI/progress_bar/bar_half_orange.png";
           bars[2].src="../traitement_inscription/UI/progress_bar/bar.png";
           step2.src="../traitement_inscription/UI/progress_bar/2_orange.png";
           step3.src="../traitement_inscription/UI/progress_bar/3.png";
           step4.src="../traitement_inscription/UI/progress_bar/4.png";
        }
        else
        alert(xmlhttp.responseText);

        }

    }
     xmlhttp.open("POST","../traitement_inscription/etape1v2.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("r_sociale=" + raison_soc +"&tel_fixe=" +fixe+"&gsm="+gsm+"&email="+email+"&rc="+rc+"&ville="+ ville_in +  "&cndp="+c_terms+"&prsp="+data_use+"&type_ent="+opt.value.substr(1,1));
               }


           }


        //   switch(type_credit){
        //       case 'M' : document.getElementById("form_simulation_mobilier").style.display="block";document.getElementById("form_simulation_immobilier").style.display="none";break;
        //       case 'I' : document.getElementById("form_simulation_immobilier").style.display="block";
        //       document.getElementById("form_simulation_mobilier").style.display="none";break;
        //   }

       }

       function etape2(mnt,dure,loyer,vr,avn){
           var taux;
           var t_invest=opt.value.substr(2,1);
           var gsm=document.getElementById("gsm_mobilier").value;
            var tb=document.getElementById("type_bien");
           var t_bien=tb.options[tb.selectedIndex].value;

           switch(t_invest){
               case '0' : taux=8;break;
               case '1' : taux=7.25;break;
               case '2' : taux=6.50;break;
           }
            var xmlhttp= window.XMLHttpRequest ?
 new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

 xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
    {
        if(xmlhttp.responseText=="done"){

        }
        else{
            alert(xmlhttp.responseText);
        }

    }

    }



    xmlhttp.open("POST","../traitement_inscription/etape2.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("montant=" + mnt+"&taux="+taux+"&vr="+vr+"&loye_ttc="+loyer+"&avance="+avn+"&duree="+dure+ "&gsm="+gsm+"&type_bien="+t_bien +"&bien="+t_invest);



       }


       var wich="N/A";

       function docs(){

           var gsm=document.getElementById("gsm_mobilier").value;
           var sa=document.getElementById("secteur_activite");
           var sec_activite=sa.options[sa.selectedIndex].value;
        //   var tb=document.getElementById("type_bien");
        //   var t_bien=tb.options[tb.selectedIndex].value;
        //   var c_affaire=document.getElementById("chiffre_affaire").value;
           var adr_pro=document.getElementById("adress_p").value;
           var vils=document.getElementById("ville_identification2");
           var ville_in=vils.options[vils.selectedIndex].value;
           var choix ;
           if(document.getElementById("oui").checked==true)
           choix="oui";
           else
           choix="non";
           var banks=document.getElementById("choix_banque");
           var banque=banks.options[banks.selectedIndex].value;
           var agence_in;
           var agencies;
           if(banque=="Attijariwafa Bank"){
           agencies=document.getElementById("choix_agence");
           agence_in=agencies.options[agencies.selectedIndex].text;//changer valeur par la suite

           }
           else
           agence_in="9999";
        //     var type_client=opt.value.substr(1,1);
        //   switch(type_client){
            //   case 'E' : document.getElementById("ent_dossier").style.display="block";document.getElementById("pp_dossier").style.display="none";break;
            //   case 'L' : document.getElementById("pp_dossier").style.display="block";document.getElementById("ent_dossier").style.display="none";break;
        //   }
            var xmlhttp= window.XMLHttpRequest ?
 new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

 xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
    {

        if(xmlhttp.responseText=='done'){
            //call function here
            dossier_jur();
         document.getElementById("joindre_docs").style.display="block";
           document.getElementById("Identification2").style.display="none";
           texts[2].style.color="#ec9102";
            texts[3].style.color="#ec9102";
           bars[0].src="../traitement_inscription/UI/progress_bar/bar_orange.png";
           bars[1].src="../traitement_inscription/UI/progress_bar/bar_orange.png";
           bars[2].src="../traitement_inscription/UI/progress_bar/bar_orange.png";
           step3.src="../traitement_inscription/UI/progress_bar/3_orange.png";
            step4.src="../traitement_inscription/UI/progress_bar/4_orange.png";
        }
        else
        alert(xmlhttp.responseText);

    }

    }



    xmlhttp.open("POST","../traitement_inscription/etape3.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("gsm=" + gsm +"&secteur_activite="+sec_activite+"&adr_pro="+adr_pro+"&ville="+ville_in+"&client="+choix+"&banque="+banque+"&agence="+agence_in);





       }

       function dossier_jur(){
            var type_client=opt.value.substr(1,1);
            var e = document.getElementById("m_type");

     var type_bien = e.options[e.selectedIndex].value;
            var xmlhttp= window.XMLHttpRequest ?
 new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

 xmlhttp.onreadystatechange = function() {
 	if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
            document.getElementById("doss").innerHTML=xmlhttp.responseText;
    }
    xmlhttp.open("POST","../traitement_inscription/dossier_jur.php",true);//php file from server
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("type_client="+type_client +"&type_bien="+type_bien);
       }

       function fill_cities(id_sel){
              var xmlhttp= window.XMLHttpRequest ?
 new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

 xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
    {
        //  alert(xmlhttp.responseText);
        document.getElementById(id_sel).innerHTML=xmlhttp.responseText;

    }

    }



    xmlhttp.open("GET","../traitement_inscription/villes_filling.php",true);

    xmlhttp.send();

       }

    function set_type_bien(){
        var type_c=opt.value.substr(2,1);
        //  var goodies=document.getElementById("type_bien");


            var xmlhttp= window.XMLHttpRequest ?
 new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");

 xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
    {
        //  alert(xmlhttp.responseText);
        document.getElementById("type_bien").innerHTML=xmlhttp.responseText;

    }

    }
    xmlhttp.open("POST","../traitement_inscription/biens.php",true);
    xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xmlhttp.send("type_credit=" + type_c);

    }
