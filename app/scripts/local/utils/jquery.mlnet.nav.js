var mlnetNav = mlnetNav || {};

mlnetNav = {
    navMenuSelector : "",
    init: function() {
        this.navMenuSelector = jQuery(".");
        this._eventHandler();
    },

    _eventHandler: function() {
        var self = this;
        self.navMenuSelector.click(function() {
            if(jQuery('.closed_nav').length) jQuery('.closed_nav').removeClass('closed_nav');
            else
            jQuery('body').toggleClass('closed_nav');
        });
    }
};
