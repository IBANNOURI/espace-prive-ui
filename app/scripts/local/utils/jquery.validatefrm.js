var mlnetForm = mlnetForm || {};

mlnetForm = {
    $formSelector: jQuery('.j-hasValidation'),
    $icoShowPassword: jQuery('.j-toggle-password-on'),
    $fieldPassword: jQuery('.j-password_field'),
    init: function () {
        this.$formSelector = jQuery('.j-hasValidation');
        this._eventHandler();
        this._initialState();
    },

    _eventHandler: function () {
        var self = this;
        jQuery(".j-cards-toslide_action").click(function(){
            jQuery(".js-card-toslide_content").toggleClass("toggle");
            return false;
        });
    },
    _initialState: function () {
        var self = this;
        if (!self.$formSelector.length) return false;
        jQuery("document").trigger('click');
        var formValidationI18nFormat = {
            maxlength: jQuery.validator.format("Veuillez fournir au plus {0} caractères."),
            minlength: jQuery.validator.format("Veuillez fournir au moins {0} caractères."),
            rangelength: jQuery.validator.format("Veuillez fournir une valeur qui contient entre {0} et {1} caractères."),
            range: jQuery.validator.format("Veuillez fournir une valeur entre {0} et {1}."),
            max: jQuery.validator.format("Veuillez fournir une valeur inférieure ou égale à {0}."),
            min: jQuery.validator.format("Veuillez fournir une valeur supérieure ou égale à {0}."),
            step: jQuery.validator.format("Veuillez fournir une valeur multiple de {0}."),
            maxWords: jQuery.validator.format("Veuillez fournir au plus {0} mots."),
            minWords: jQuery.validator.format("Veuillez fournir au moins {0} mots."),
            rangeWords: jQuery.validator.format("Veuillez fournir entre {0} et {1} mots."),
            strippedminlength: jQuery.validator.format("Veuillez fournir au moins {0} caractères."),
            require_from_group: jQuery.validator.format("Veuillez fournir au moins {0} de ces champs.")
        };
        jQuery.extend(jQuery.validator.messages, Object.assign(i18n.fr.translations.common.formvalidation, formValidationI18nFormat));
        jQuery.validator.addMethod('c_password', function (value) {
            if (value == jQuery('#password').val()) {
                return true;
            }
            return false;
        }, i18n.fr.translations.common.formvalidation.equalTo);
        jQuery.validator.addMethod("email", function(value, element, param) {
            return value.match(/^(\s?[^\s,]+@[^\s,]+\.[^\s,]+\s?,)*(\s?[^\s,]+@[^\s,]+\.[^\s,]+)$/);
        });
        self.$formSelector.each(function (i, elt) {
            jQuery(this).validate({
                errorElement: 'span',
                rules: {},
                highlight: function (element, errorClass, validClass) {
                    jQuery(element).parent().addClass('error').removeClass("valide");
                    jQuery(element).addClass('error').removeClass("valide");
                },
                unhighlight: function (element, errorClass, validClass) {
                    jQuery(element).parent().removeClass('error').addClass("valide");
                    jQuery(element).removeClass('error').addClass('valide');
                },
                errorPlacement: function (error, element) {
                    jQuery(element).parent().after(error);
                }/*,
                submitHandler: function (form) {
                    jQuery(form).submit();
                    var result = { };
                    jQuery.each(jQuery(form).serializeArray(), function() {
                        result[this.name] = this.value;
                    });

                    return false;
                    if (jQuery(form).attr("id") == "") {

                    }
                    return false;
                }*/
            });
        });


    }

};