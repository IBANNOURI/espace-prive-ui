// Uses CommonJS, AMD or browser globals to create a jQuery plugin.
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = function( root, jQuery ) {
            if ( jQuery === undefined ) {
                // require('jQuery') returns a factory that requires window to
                // build a jQuery instance, we normalize how we use modules
                // that require this pattern but the window provided is a noop
                // if it's defined (how jquery works)
                if ( typeof window !== 'undefined' ) {
                    jQuery = require('jquery');
                }
                else {
                    jQuery = require('jquery')(root);
                }
            }
            factory(jQuery);
            return jQuery;
        };
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {
    /**
     * jquery.mlnet.applicationlication.js
     * contain global project js
     */
    var dbmApp;

    dbmApp = {
        init : function() {
            this._wysiwyg();
            this._fitVids();
        },

         _wysiwyg: function() {
            $('.c-wysiwyg iframe').wrap('<div class="js-fitvids"></div>');
            $('.c-wysiwyg table').wrap('<div class="c-table__wrapper"/>');
        },

        _fitVids: function() {
            $('.js-fitvids').fitVids();
        }
    };

    $(document).ready(function() {
        dbmApp.init();
    });
}));
