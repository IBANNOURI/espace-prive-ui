var mlnetGoToTop;

mlnetGoToTop = {
    $goToTop: jQuery('.c-action__gototop'),

    init: function () {
        this._eventHandler();
        this._waypoints();
    },

    _eventHandler: function () {
        var self = this;

        this.$goToTop.on('click', function (ev) {
            ev.preventDefault();
            self._goToTop();
        });
    },

    _waypoints: function () {
        var self = this;

        if (jQuery('.c-action__gototop').length > 0) {
            jQuery('body').waypoint({
                handler: function () {
                    self.$goToTop.toggleClass('is-visible');
                },
                offset: -85
            });
        }
    },

    _goToTop: function () {
        this.$goToTop.mlnetScrollto({
            scrollTarget: jQuery('body'),
            scrollSpeed: 400
        });
    }
};