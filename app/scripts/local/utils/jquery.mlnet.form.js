var mlnetForm = mlnetForm || {};
var mlnetDynamicForm = mlnetDynamicForm || {};


mlnetForm = {
    $formSelector: jQuery('.j-hasValidation'),
    $icoShowPassword: jQuery('.j-toggle-password-on'),
    $fieldPassword: jQuery('.j-password_field'),
    data : [],
    init: function () {
        this.$formSelector = jQuery('.j-hasValidation');
        this._eventHandler();
        this._initialState();

        this.noUiSliderInit();

       this.data = [
                {

                },
                {id:"XR2",text:"TERR AMORT",tauxvr:10},
                {id:"MB06",text:"AMENAGEMENTS",tauxvr:1},
                {id:"MD10",text:"EQUIPEMENT MEDICAL",tauxvr:1},
                {id:"MT07",text:"Carrosserie",tauxvr:1},
                {id:"TP22",text:"Equipements TP",tauxvr:1},
                {id:"M05C",text:"MATERIEL DE PRODUCTION",tauxvr:1},
                {id:"IB",text:"IMMEUBLE BUREAU",tauxvr:10},
                {id:"II",text:"IMMEUBLE INDUSTRIEL",tauxvr:10},
                {id:"LCA",text:"LOCAL COMMERCIAL",tauxvr:10},
                {id:"MG",text:"MAGASINS",tauxvr:10},
                {id:"PBA",text:"PLATEAU DE BUREAU",tauxvr:10},
                {id:"B02A",text:"MATERIEL INFORMATIQUE",tauxvr:1},
                {id:"R02C",text:"ROULANT POIDS LOURD",tauxvr:1},
                {id:"VT01",text:"VEHICULE DE TOURISME",tauxvr:1},
                {id:"MB03",text:"MOBILIER BUREAUTIQUE",tauxvr:1},
                {id:"IM01",text:"CONSTRUCTION",tauxvr:10},
                {id:"VU02",text:"VEHICULE UTILITAIRE",tauxvr:1},
                {id:"MP06",text:"EQUIPEMENT",tauxvr:1},
                {id:"MT02",text:"CAMION",tauxvr:1},
                {id:"MT03",text:"CAMION + BENNE (OU PLATEAU)",tauxvr:1},
                {id:"MT05",text:"SEMI-REMORQUE",tauxvr:1},
                {id:"MT06",text:"TRACTEUR ROUTIER",tauxvr:1}

        ];

        this.__initSelectCustom(this.data);

        var  rangeToPercent = function(number, min, max){
            return ((number - min) / (max - min));
        };
        var percentToRange = function (percent, min, max) {
            return((max - min) * percent + min);
        };

        jQuery('body').on('click', '.js-rangePercent button', function () {
            jQuery('.js-rangePercent button').removeClass("selected");
            jQuery(this).addClass("selected");
         //  if(jQuery(this).text() == "%") rangeToPercent();
          // else percentToRange();

        })
    },
    noUiSliderInit : function () {


    },
    __initSelectCustom : function(data){
        var _self = this;
        jQuery(".js-select2").not(".no-data").select2({
            data: data,
            width: 'auto'
        });

    },

    _eventHandler: function () {
        var self = this;
        var dataFrais =  [
            {
                id: "MATE",
                text: ' Matériel'
            },{
                id: "ACCS",
                text: ' Accessoires (BL)'
            },{
                id: "ATFR",
                text: ' Autres frais (BL)'
            },{
                id: "FRIM",
                text: ' Frais d\'immatriculation (BL)'
            },{
                id: "OPTO",
                text: ' Options (BL)'
            }
        ];
        var addNewRow = function (templateSelect, containerSelect) {
            console.log(containerSelect)
            var _rowCount = containerSelect.find("tr").length;
            var template = templateSelect.html();
            var upgradeId = function () {
                return jQuery(templateSelect).find('[id="{{id}}"]').each(function (index, item) {
                    jQuery(this).replace(/{{id}}/gi, _rowCount + 'input-' + index + '-' + new Date().getTime());
                });
            };
            console.log(upgradeId())
            var _newRow = upgradeId();
            //jQuery(template).insertAfter(containerSelect.find('.editable-row__template').last()).show();

            containerSelect.find(">tbody").append(template);
            upgradeAllRegistered();

            self.__initSelectCustom(dataFrais);
        };

        jQuery('body').on('click', '.j-cta-product_sup', function () {
            var elt = jQuery(this);
            elt.toggleClass('toggled-active');
            elt.parents("tr").toggleClass("toggled");
            if (elt.parents("tr").next(".j-product_sup").find(".j-productFrais_push").find("tbody").find("tr").length == 0) addNewRow(jQuery("#productFraisTemplate"), elt.parents("tr").next(".j-product_sup").find(".j-productFrais_push"));
            elt.parents("tr").next(".j-product_sup").find(".sub-table-wrap").slideToggle("fast");
            return false;
        });

        var upgradeAllRegistered = function () {
            jQuery('.editable-row__template').each(function (index, elt) {
                var switchField = jQuery(this).find('input[name*="type"]');
                switchField.attr("id", switchField.attr("id") + index);
                switchField.next('label').attr("for", switchField.attr("id"));
            });
        }

        jQuery('body').on('click', '.j-add-row', function (ev) {
            /* jQuery('.editable-row__template').first().clone().insertAfter(jQuery('.editable-row__template').last()).show();
             jQuery('.editable-row__template').find(".j-delete-row").removeClass("hide");
             upgradeAllRegistered();*/
            var elt = jQuery(this);
            //jQuery("#productTemplate"), jQuery(".j-product_push"))
            addNewRow(jQuery("#" + jQuery(elt).data("tpltarget")), elt.parents("table").prev("table"));
            return false;

        });


        jQuery('body').on('click', '.j-delete-row', function (ev) {
            var elt = jQuery(this);
            if (elt.parents('table').find('tbody > tr.editable-row__template').length > 1) {
                elt.parent('td').parent('tr').next("tr").remove();
                elt.closest('.editable-row__template').remove();
            }
            return false;

        });
        jQuery('body').on('click', '.j-product-quantity button', function (ev) {
            var elementContainer = jQuery(this).parent();
            var currentQty = elementContainer.find('input[name*="quantity"]').val();
            var qtyDirection = jQuery(this).data("direction");
            var newQty = 0;

            if (qtyDirection == "1") {
                newQty = parseInt(currentQty) + 1;
            } else if (qtyDirection == "-1") {
                newQty = parseInt(currentQty) - 1;
            }

            // make decrement disabled at 1
            if (newQty == 1) {
                elementContainer.find(".decrement-quantity").attr("disabled", "disabled");
            }

            // remove disabled attribute on subtract
            if (newQty > 1) {
                elementContainer.find(".decrement-quantity").removeAttr("disabled");
            }

            if (newQty > 0) {
                newQty = newQty.toString();
                elementContainer.find('input[name*="quantity"]').val(newQty);
            } else {
                elementContainer.find('input[name*="quantity"]').val("1");
            }

        });

        jQuery(".can-toggle_btn-witcher .can-toggle_btn").click(function () {
            jQuery(".can-toggle_btn-witcher .can-toggle_btn").removeClass("checked")
            jQuery(this).addClass("checked");
            jQuery(this).parent().toggleClass("checked");
            return false;
        })
        var dataForm =
            {
                "Raison sociale": {
                    "type": "text",
                    "placeholder": "Raison sociale",
                    "required": true,
                    "typePersonne": [
                        "personnemorale"
                    ]
                },
                "Forme juridique": {
                    "type": "select",
                    "required": true,
                    "enum": [
                        "SA",
                        "SARL"
                    ],
                    "typePersonne": [
                        "personnemorale"
                    ]
                },
                "ICE": {
                    "type": "text",
                    "placeholder": "123456789000057",
                    "required": true,
                    "typePersonne": [
                        "personnemorale"
                    ]
                },
                "CIN": {
                    "type": "text",
                    "required": true,
                    "placeholder": "AB616701",
                    "typePersonne": [
                        "personnephysique",
                        "professionLiberale"
                    ]
                },
                "contact": {
                    "type": "text",
                    "placeholder": "Nom et prénom",
                    "required": true,
                    "typePersonne": [
                        "personnemorale",
                        "personnephysique",
                        "professionLiberale"
                    ]
                },
                "Type de contact": {
                    "type": "text",
                    "typePersonne": [
                        "personnemorale",
                    ]
                },
                "Téléphone": {
                    "type": "text",
                    "required": true,
                    "placeholder": "(00212) 6 66 62 87 51",
                    "typePersonne": [
                        "personnemorale",
                        "personnephysique",
                        "professionLiberale"
                    ]
                },
                "E-mail": {
                    "type": "text",
                    "required": true,
                    "email": true,
                    "placeholder": "exemple@exemple.com",
                    "typePersonne": [
                        "personnemorale",
                        "personnephysique",
                        "professionLiberale"
                    ]
                }
            };
        var inputText = '<input type="{type}" id="{id}" name="{name}" class="{required}{email}" placeholder="{placholder}" />';
        jQuery(".j-change-form").find("input").click(function () {
            var $this = jQuery(this)
            jQuery(".tab-form").attr("id", $this.attr("id") + "tab");
        })

        /*self.$icoShowPassword.mouseover(function () {
            self.$fieldPassword.attr("type", "text")
        }).mouseout(function () {
            self.$fieldPassword.attr("type", "password")
        })*/
        jQuery("tr").click(function () {
            jQuery("tr").removeClass("focused");
            jQuery(this).addClass("focused");
        })
        const $menu = jQuery('tr');

        jQuery(document).mouseup(function (e) {
            if (!$menu.is(e.target) // if the target of the click isn't the container...
                && $menu.has(e.target).length === 0 && !$menu.find('input[type="radio"]').length) // ... nor a descendant of the container
            {
                $menu.removeClass('focused');
            }
        });

        jQuery(".j-checked-tovalidate").find('input[type="radio"]').parents("tr").click(function () {
            jQuery(this).find('input[type="radio"]').prop("checked", true);
            jQuery(this).parents("form").find(":submit").prop("disabled", false);
        })
        jQuery(".j-checked-tovalidate").find('input[type="radio"]').click(function () {
            jQuery(this).parents("form").find(":submit").prop("disabled", false);
        })
        self.$icoShowPassword.click(function () {
            var $this = jQuery(this)
            if ($this.parent().find(self.$fieldPassword).attr("type") == 'text') {
                $this.parent().find(self.$fieldPassword).attr("type", "password");
                $this.removeClass('showpassword');
            } else {
                $this.parent().find(self.$fieldPassword).attr("type", "text");
                $this.addClass('showpassword');
            }
        })


    },
    _initialState: function () {
        var self = this;
        if (!self.$formSelector.length) return false;

        jQuery("document").trigger('click');
        var formValidationI18nFormat = {
            maxlength: jQuery.validator.format("Veuillez fournir au plus {0} caractères."),
            minlength: jQuery.validator.format("Veuillez fournir au moins {0} caractères."),
            rangelength: jQuery.validator.format("Veuillez fournir une valeur qui contient entre {0} et {1} caractères."),
            range: jQuery.validator.format("Veuillez fournir une valeur entre {0} et {1}."),
            max: jQuery.validator.format("Veuillez fournir une valeur inférieure ou égale à {0}."),
            min: jQuery.validator.format("Veuillez fournir une valeur supérieure ou égale à {0}."),
            step: jQuery.validator.format("Veuillez fournir une valeur multiple de {0}."),
            maxWords: jQuery.validator.format("Veuillez fournir au plus {0} mots."),
            minWords: jQuery.validator.format("Veuillez fournir au moins {0} mots."),
            rangeWords: jQuery.validator.format("Veuillez fournir entre {0} et {1} mots."),
            strippedminlength: jQuery.validator.format("Veuillez fournir au moins {0} caractères."),
            require_from_group: jQuery.validator.format("Veuillez fournir au moins {0} de ces champs.")
        };
        jQuery.extend(jQuery.validator.messages, Object.assign(i18n.fr.translations.common.formvalidation, formValidationI18nFormat));
        jQuery.validator.addMethod('c_password', function(value){ if(value == jQuery('#password').val()){ return true; } return false; }, i18n.fr.translations.common.formvalidation.equalTo);
        self.$formSelector.each(function (i, elt) {
            jQuery(this).validate({
                errorElement: 'span',
                rules: {
                },
                highlight: function (element, errorClass, validClass) {
                    jQuery(element).parent().addClass('error').removeClass("valide");
                    jQuery(element).addClass('error').removeClass("valide");
                },
                unhighlight: function (element, errorClass, validClass) {
                    jQuery(element).parent().removeClass('error').addClass("valide");
                    jQuery(element).removeClass('error').addClass('valide');
                },
                errorPlacement: function (error, element) {
                    jQuery(element).parent().after(error);
                }/*,
                submitHandler: function (form) {
                    jQuery(form).submit();
                    var result = { };
                    jQuery.each(jQuery(form).serializeArray(), function() {
                        result[this.name] = this.value;
                    });

                    return false;
                    if (jQuery(form).attr("id") == "") {

                    }
                    return false;
                }*/
            });
        });


    }

};