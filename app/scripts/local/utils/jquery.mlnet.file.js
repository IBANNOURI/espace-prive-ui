// Uses CommonJS, AMD or browser globals to create a jQuery plugin.
(function (factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(['jquery'], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node/CommonJS
        module.exports = function( root, jQuery ) {
            if ( jQuery === undefined ) {
                // require('jQuery') returns a factory that requires window to
                // build a jQuery instance, we normalize how we use modules
                // that require this pattern but the window provided is a noop
                // if it's defined (how jquery works)
                if ( typeof window !== 'undefined' ) {
                    jQuery = require('jquery');
                }
                else {
                    jQuery = require('jquery')(root);
                }
            }
            factory(jQuery);
            return jQuery;
        };
    } else {
        // Browser globals
        factory(jQuery);
    }
}(function ($) {
    /**
     * jquery.mlnet.file.js
     * contain file js
     */
    var dbmFile;

    dbmFile = {
        init: function() {
            this._eventHandler();
        },

        _eventHandler: function() {
            var self = this;

            $('input[type="file"]').each(function() {
                $(this).on('change', function($el) {
                    self._file($(this));
                });
            });
        },

        _file: function($el) {
            var $file = $el,
                label = $file.val(),
                result = label.replace('C:\\fakepath\\', '');

            if (result.length > 35) {
                result = result.substr(0, 35) + '...';
            }

            $file.closest('.c-form__item--file').find('input[type="text"]').val(result);
        },
    };

    $(document).ready(function() {
        dbmFile.init();
    });
}));
