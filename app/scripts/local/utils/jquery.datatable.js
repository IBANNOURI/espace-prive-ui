var mlnetDatatables = mlnetDatatables || {};

mlnetDatatables = {
    $dataTableSelector: jQuery('.j-datatable'),
    init: function() {
        this._eventHandler();
        //this._initialState();
    },
    _eventHandler: function() {
        var self =  this;
        jQuery('.js-toggle-row').click(function () {
            if(!jQuery(this).parents("tr").next(".toggle-row").length) return false;
            jQuery('.js-toggle-row').not(jQuery(this)).removeClass('active');
            jQuery(this).parents("tr").toggleClass('actived-row');
            jQuery(this).toggleClass('active').parents("tr").next().find('.sub-table-wrap').slideToggle();
            jQuery(".toggle-row").not(jQuery(this).parents("tr").next()).find('.sub-table-wrap').slideUp('fast');
            return false;
        });

    },
    _initialState: function() {
        var self =  this;
        jQuery('.j-datatable').DataTable({
            'responsive': true,
            searching : false,
            "pagingType": "simple_numbers",
            "dom": '<"top"i>rt<"bottom"fpl><"clear">',
            "oLanguage": {
                "sLengthMenu": "_MENU_",
            },
            "columnDefs": [
                {
                    "targets": 'no-sort',
                    "orderable": false,
                }
            ]
        }).columns.adjust();

    }

};
