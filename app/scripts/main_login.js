require.config({
    baseURL: '.',
    paths: {
        conf: 'conf/appconf',
        i18n: 'i18n/fr',
        jquery: 'libs/jquery/jquery.min',
        validate: 'libs/formvalidate/jquery.validate.min',
        'jquery.validate.addon': "libs/formvalidate/additional-methods.min",
        mlnetForm : "local/utils/jquery.validatefrm",
        mlnetAlert : "local/utils/jquery.mlnet.alert",
        application : "local/application_login"
    },
    shim: {
        conf : {
            exports: 'conf'
        },
        offers : {
            exports: 'offers'
        },
        jquery: {
            deps: [],
            exports: '$',
            init: function () {
                return window.jQuery = $;
            }
        },
        validate: {
            exports: 'validate',
            deps: ['jquery']
        },
        'jquery.validate.addon': {
            exports :'jquery.validate.addon',
            deps: ['validate']
        },
        i18n : {
            exports: 'i18n',
            deps: ['jquery', 'validate']
        },
        application: {
            exports: 'application',
            deps:  ['validate',
                'mlnetAlert',
                'mlnetForm']
        },
        /*Modernizr: {
            exports: 'Modernizr'
        },
        Detectizr: {
            exports: 'Detectizr'
        },
        waypoint : {
            exports: 'waypoint',
            deps: ['jquery']
        }*/
    }
});

require([
    // Load our application module and pass it to our definition function
    'app_login',
], function (App) {
    // The "application" dependency is passed in as "App"
    App.initialize();
});