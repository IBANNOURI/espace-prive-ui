define([
    'conf',
    'jquery',
    'validate',
    'i18n',
    'application',
], function () {
    var initialize = function(){
        // use jQuery Function for selector, dont use $
        $.noConflict();

        // Forms initialize
        application.mlnetforms.init();

    }
    return {
        initialize: initialize
    };
});