var offers = [
    {
        "id": 'offer-1',
        "path": '/assets/media/images/offers',
        "thumbImg": "/assets/media/images/offers/libertis-pro.jpg",
        "title": "LIBERTIS PRO",
        "subTitle": "LA SOLUTION BANCAIRE COMPLÈTE DES PROFESSIONS LIBÉRALES",
        "summary": "Vous n’aurez plus à jongler entre besoins Pro et Perso.",
        "plaquette": "medicalleasing.pdf",
        "fields": [
            {
                "label": "Nature de bien",
                "name": "NatureDeBien",
                "type": "select",
                "placeholder": "Rensigner la Nature de bien",
                "required": true,
                "disabled": false,
                "data": [
                    {"id": "XR2", "label": "Rensigner la Nature de bien", "value": "Rensigner la Nature de bien"},
                    {"id": "XR2", "label": "TERR AMORT", "value": "TERR AMORT"},
                    {"id": "MB06", "label": "AMENAGEMENTS", "value": "AMENAGEMENTS"},
                    {"id": "MD10", "label": "EQUIPEMENT MEDICAL", "value": "EQUIPEMENT MEDICAL"},
                    {"id": "MT07", "label": "Carrosserie", "value": "Carrosserie"}
                ]
            },
            {
                "label": "Tranche Chiffre d'affaire",
                "name": "ca",
                "type": "select",
                "placeholder": "Choisir le chiffre d'affaire",
                "required": true,
                "disabled": false,
                "data": [
                    {"id": "1", "label": "Entre 10M et 20M", "value": "Entre 10M et 20M"},
                    {"id": "2", "label": "Entre 20M et 23M", "value": "Entre 20M et 23M"},
                    {"id": "3", "label": "Entre 30M et 40M", "value": "Entre 30M et 40M"}
                ]
            }
        ]
    },
    {
        "id": 'offer-2',
        "thumbImg": "/assets/media/images/offers/almoukawil-chaabi-lease.jpg",
        "title": "AL MOUKAWIL CHAABI LEASE",
        "subTitle": "Le leasing dédié aux petites entreprises",
        "summary": "Vous etes un professionnel de la santé(*) et souhaitez acquérir un bien d'équipement ou un immobilier professionnel, Maroc Leasing vous propose MEDICAL LEASING, La solution leasing la mieux adaptée à vos besoin et à votre métier.",
        "content": "",
        "plaquette": "agrolieasing.pdf",
        "params": [
            {
                "label": "Nature de bien",
                "name": "NatureDeBien",
                "field": "select",
                "type": "",
                "placeholder": "Rensigner la Nature de bien",
                "data": [
                    {"id": "XR2", "text": "Rensigner la Nature de bien", "tauxvr": 10},
                    {"id": "XR2", "text": "TERR AMORT", "tauxvr": 10},
                    {"id": "MB06", "text": "AMENAGEMENTS", "tauxvr": 1},
                    {"id": "MD10", "text": "EQUIPEMENT MEDICAL", "tauxvr": 1},
                    {"id": "MT07", "text": "Carrosserie", "tauxvr": 1},
                    {"id": "TP22", "text": "Equipements TP", "tauxvr": 1},
                    {"id": "M05C", "text": "MATERIEL DE PRODUCTION", "tauxvr": 1},
                    {"id": "IB", "text": "IMMEUBLE BUREAU", "tauxvr": 10},
                    {"id": "II", "text": "IMMEUBLE INDUSTRIEL", "tauxvr": 10},
                    {"id": "LCA", "text": "LOCAL COMMERCIAL", "tauxvr": 10},
                    {"id": "MG", "text": "MAGASINS", "tauxvr": 10},
                    {"id": "PBA", "text": "PLATEAU DE BUREAU", "tauxvr": 10},
                    {"id": "B02A", "text": "MATERIEL INFORMATIQUE", "tauxvr": 1},
                    {"id": "R02C", "text": "ROULANT POIDS LOURD", "tauxvr": 1},
                    {"id": "VT01", "text": "VEHICULE DE TOURISME", "tauxvr": 1},
                    {"id": "MB03", "text": "MOBILIER BUREAUTIQUE", "tauxvr": 1},
                    {"id": "IM01", "text": "CONSTRUCTION", "tauxvr": 10},
                    {"id": "VU02", "text": "VEHICULE UTILITAIRE", "tauxvr": 1},
                    {"id": "MP06", "text": "EQUIPEMENT", "tauxvr": 1},
                    {"id": "MT02", "text": "CAMION", "tauxvr": 1},
                    {"id": "MT03", "text": "CAMION + BENNE (OU PLATEAU)", "tauxvr": 1},
                    {"id": "MT05", "text": "SEMI-REMORQUE", "tauxvr": 1},
                    {"id": "MT06", "text": "TRACTEUR ROUTIER", "tauxvr": 1}
                ]
            },
            {
                "label": "Secteur d'activité",
                "field": "select",
                "type": "",
                "placeholder": "Rensigner le Secteur d'activité",
                "data": [
                    {
                        "id": "MATE",
                        "text": ' Rensigner le Secteur d\'activité'
                    }, {
                        "id": "MATE",
                        "text": ' Agroalimentaire'
                    }, {
                        "id": "ACCS",
                        "text": ' Banque / Assurance'
                    }, {
                        "id": "ATFR",
                        "text": ' Bois / Papier / Carton / Imprimerie'
                    }, {
                        "id": "FRIM",
                        "text": ' BTP / Matériaux de construction'
                    }, {
                        "id": "OPTO",
                        "text": ' Chimie / Parachimie'
                    }, {
                        "id": "OPTO",
                        "text": ' Commerce / Négoce / Distribution'
                    }, {
                        "id": "OPTO",
                        "text": ' Édition / Communication / Multimédia'
                    }, {
                        "id": "OPTO",
                        "text": ' Électronique / Électricité'
                    }
                ]
            },
            {
                "label": "Chiffre d'affaire 2019",
                "field": "input",
                "type": "number",
                "placeholder": "Rensigner le chiffre d'affaire",
                "data": []
            }
        ]
    },
    {
        "id": 'offer-3',
        "thumbImg": "/assets/media/images/offers/industry.jpg",
        "title": "Industriels Leasing",
        "subTitle": "La solution leasing sur mesure pour les professionnels de la santé",
        "summary": "Vous etes un professionnel de la santé(*) et souhaitez acquérir un bien d'équipement ou un immobilier professionnel, Maroc Leasing vous propose MEDICAL LEASING, La solution leasing la mieux adaptée à vos besoin et à votre métier.",
        "content": "",
        "plaquette": "industry.pdf",
        "params": [
            {
                "label": "Nature de bien",
                "field": "select",
                "name": "NatureDeBien",
                "type": "",
                "placeholder": "Rensigner la Nature de bien",
                "data": [
                    {"id": "XR2", "text": "Rensigner la Nature de bien", "tauxvr": 10},
                    {"id": "XR2", "text": "TERR AMORT", "tauxvr": 10},
                    {"id": "MB06", "text": "AMENAGEMENTS", "tauxvr": 1},
                    {"id": "MD10", "text": "EQUIPEMENT MEDICAL", "tauxvr": 1},
                    {"id": "MT07", "text": "Carrosserie", "tauxvr": 1},
                    {"id": "TP22", "text": "Equipements TP", "tauxvr": 1},
                    {"id": "M05C", "text": "MATERIEL DE PRODUCTION", "tauxvr": 1},
                    {"id": "IB", "text": "IMMEUBLE BUREAU", "tauxvr": 10},
                    {"id": "II", "text": "IMMEUBLE INDUSTRIEL", "tauxvr": 10},
                    {"id": "LCA", "text": "LOCAL COMMERCIAL", "tauxvr": 10},
                    {"id": "MG", "text": "MAGASINS", "tauxvr": 10},
                    {"id": "PBA", "text": "PLATEAU DE BUREAU", "tauxvr": 10},
                    {"id": "B02A", "text": "MATERIEL INFORMATIQUE", "tauxvr": 1},
                    {"id": "R02C", "text": "ROULANT POIDS LOURD", "tauxvr": 1},
                    {"id": "VT01", "text": "VEHICULE DE TOURISME", "tauxvr": 1},
                    {"id": "MB03", "text": "MOBILIER BUREAUTIQUE", "tauxvr": 1},
                    {"id": "IM01", "text": "CONSTRUCTION", "tauxvr": 10},
                    {"id": "VU02", "text": "VEHICULE UTILITAIRE", "tauxvr": 1},
                    {"id": "MP06", "text": "EQUIPEMENT", "tauxvr": 1},
                    {"id": "MT02", "text": "CAMION", "tauxvr": 1},
                    {"id": "MT03", "text": "CAMION + BENNE (OU PLATEAU)", "tauxvr": 1},
                    {"id": "MT05", "text": "SEMI-REMORQUE", "tauxvr": 1},
                    {"id": "MT06", "text": "TRACTEUR ROUTIER", "tauxvr": 1}
                ]
            },
            {
                "label": "Chiffre d'affaire 2019",
                "field": "input",
                "type": "number",
                "placeholder": "Rensigner le chiffre d'affaire",
                "data": []
            }
        ]
    },
    {
        "id": 'offer-4',
        "thumbImg": "/assets/media/images/offers/construction.jpg",
        "title": "Construction Leasing",
        "subTitle": "La solution leasing sur mesure pour les professionnels de la santé",
        "summary": "Vous etes un professionnel de la santé(*) et souhaitez acquérir un bien d'équipement ou un immobilier professionnel, Maroc Leasing vous propose MEDICAL LEASING, La solution leasing la mieux adaptée à vos besoin et à votre métier.",
        "content": "",
        "plaquette": "construction.pdf",
        "params": [

            {
                "label": "Chiffre d'affaire 2019",
                "field": "input",
                "names": "input",
                "type": "number",
                "placeholder": "Rensigner le chiffre d'affaire",
                "data": []
            }
        ]
    }
];


var PropositionList = [
    {
        "id_proposition": '',
        "lib_proposition": '',
        "date_crea_proposition": '',
        "date_update_proposition": '',
        "montant_financement_proposition" : "",
        "Statut" : "",
        "nature_bien": "",
        //"type_propistion":"",
        "tier":
            {
                "eid": "",
                "type_client" :"",
                "rs": "",
                "nom": "",
                "prenom": "",
                "adresse": "",
                "tel": "",
                "email": ""
            }
    }
];