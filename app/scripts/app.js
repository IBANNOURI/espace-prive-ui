define([
    'conf',
    'offers',
    'jquery',
    'validate',
    'select2',
    'waypoint',
    'sticky',
    'noUiSlider',
    'Chart',
    'magnificPopup',
    //'datatables.net',
    'swal',
    'prism',
    'i18n',
    'application',
], function (conf, offers, $, validate, select2, waypoint, sticky, noUiSlider, Chart, magnificPopup, swal, prism, i18n,  application) {
    var initialize = function(){
        // use jQuery Function for selector, dont use $
        $.noConflict();

        application.layouts.init();

        // Forms initialize
        application.mlnetforms.init();

        // Modale initialize
        application.swalModals.init();

        // dataTables initialize
        application.dataTables.init();

        // dataTables initialize
        application.alert.init();

        application.simulation.init();

        if (jQuery('#donut_chart_1').length) {
            var ctx = document.getElementById("donut_chart_1").getContext("2d");
            new Chart(ctx, {
                type: 'doughnut',
                data: {
                    labels: ["LRD", 'Payés', 'Impayés'],
                    datasets: [{
                        data: [48576.47, 94553.71, 18576.4],
                        backgroundColor: [
                            "#6E97C7",
                            "#53B69C",
                            "#DE6D69"
                            //theme_color('primary'),
                            //theme_color('danger'),
                            //color(theme_color('primary')).alpha(0.4).rgbString(),
                        ],
                    }]
                },
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    legend: {
                        display: false
                    },
                    cutoutPercentage: 65,
                }
            });
        }

        if(jQuery(".j-offers-binding").length){
            var template = jQuery("#offerTemplate").html();
            var htmlRender = '<div class="row expanded">';
            jQuery.each(offers, function(i,elt){
                var html = template.replace("{{img}}", elt.thumbImg)
                    .replace("{{title}}", elt.title)
                    .replace("{{subTitle}}", elt.subTitle)
                    .replace("{{desc}}", elt.summary);
                htmlRender += '<div class="column large-6 medium-6 small-12 ">'+html+'</div>';
            })
            htmlRender+="</div>";

            jQuery(".j-offers-binding").html(htmlRender);
        }

            jQuery(".form-proposal_choice-item").find("input:radio").click(function () {
                jQuery(".form-proposal_choice-item").removeClass("selected");
                jQuery(".form-proposal_choice-item").find("select").prop("disabled", true);
                jQuery(this).parents(".form-proposal_choice-item").addClass("selected");
               jQuery(this).parents(".form-proposal_choice-item").find("select").prop("disabled", false);
                jQuery(".j-offers_binding").html("")
            })

        jQuery(".js-select2.no-data").select2({
            width: 'auto'
        });
        function createFieldSelect(fieldData) {
            var opts = '<div class="c-input__holder align-left"><label>'+fieldData.label+'</label><select id="'+fieldData.name+'" name="'+fieldData.name+'">';
            jQuery.each(fieldData.data, function (i, elt) {
                opts += '<option value="'+elt.id+'">'+elt.text+'</option>';
            })
            opts +='</select></div>'
            return opts;
        }

        function createFieldInput(fieldData) {
            var input = '<div class="c-form__item--text hasicon align-left"><label>'+fieldData.label+'</label><input type="'+fieldData.type+'" id="5" placeholder="'+fieldData.placeholder+'" value="">';
            return input;
        }

        function findObjectByKey(array, key, value) {
            for (var i = 0; i < array.length; i++) {
                if (array[i][key] === value) {
                    return array[i];
                }
            }
            return null;
        }
        jQuery(".js-select2.no-data").on('select2:select', function (e) {
            var data = e.params.data,
            value = data.id;
            var selectedData = findObjectByKey(offers,"id",value);
            var htmlRender = '<div class="form-offers form-proposal_choice-fields-inner"><div class="row small-up-1 medium-up-3 large-up-3">';
            console.log(selectedData.params)
            if(selectedData.params.length > 0){
                console.log(selectedData)
                jQuery.each(selectedData.params, function(i,elt){
                    if(elt.field == "select") htmlRender += '<div class="column">'+createFieldSelect(elt)+'</div>';
                    if(elt.field == "input") htmlRender += '<div class="column">'+createFieldInput(elt)+'</div>';
                })
                htmlRender+="</div></div>";
                jQuery(htmlRender).find("select").select2({
                    width: 'auto'
                })
                jQuery(".j-offers_binding").html(htmlRender);
            }
        });

        //Init Slider exemple

        if (!jQuery(".j-simulation_bareme").length) return false;

        var doubleHandleSlider = document.querySelector('.j-simulation_bareme');

        var minValInput = document.querySelector('.min-value');
        var maxValInput = document.querySelector('.max-value');
        var inputNumber = document.getElementById('j-input-bareme');
        var obj =

            noUiSlider.create(doubleHandleSlider, {
                start: 24,
              // step: 12,

                range: {
                    min: 24,
                    max: 72
                },
                prefix: "MOIS",
                format: {
                    from: function (value) {
                        return parseInt(value);
                    },
                    to: function (value) {
                        return parseInt(value);
                    }
                },

                // ... must be at least 300 apart
                margin: 300,



                // Display colored bars between handles
                connect: [true, false],

                // Move handle on tap, bars are draggable
                behaviour: 'tap-drag',
                tooltips: true,


                // Show a scale with the slider
                pips: {
                    mode: 'values',
                    values: [24, 36, 48, 60, 72],
                    density: 4
                }
               /* pips: {
                    mode: 'positions',
                    values: [0, 25, 50, 75, 100],
                    density: 4,
                    stepped: true
                },*/

        }).on("update.ccProjectSlider", function (t, e) {
                inputNumber.value = t[0];
            //jQuery(".slider-title span.min").text(t[0])
            //jQuery(".slider-title span.max").text(t[1])

        });


        inputNumber.addEventListener('change', function () {
            doubleHandleSlider.noUiSlider.set(this.value);
        });

    }
    return {
        initialize: initialize
    };
});