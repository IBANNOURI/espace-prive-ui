({
    baseURL: '.',
    paths: {
        conf: 'conf/appconf',
        offers: 'data/offers',
        jquery: 'libs/jquery/jquery.min',
        validate: 'libs/formvalidate/jquery.validate.min',
        'jquery.validate.addon': "libs/formvalidate/additional-methods.min",
        select2: "libs/select2/select2.min",
        swal : "libs/sweetalert/sweetalert.min",
        magnificPopup : 'vendor/jquery.magnific-popup.min',
        waypoint: 'vendor/jquery.waypoints.min',
        sticky: 'vendor/sticky.min',
        noUiSlider: 'libs/nouislider/nouislider.min',
        Chart: 'libs/chart/Chart.min',
        /*"datatables.net": "https://cdn.datatables.net/v/zf/dt-1.10.18/datatables.min",
        "datatables-markjs": "//cdn.jsdelivr.net/datatables.mark.js/2.0.0/datatables.mark.min",
        "datatables-colVis": "https://cdn.datatables.net/buttons/1.2.2/js/buttons.colVis.min",*/
        prism: 'libs/prism/prism',
        i18n: 'i18n/fr',
        mlnetForm : "local/utils/jquery.mlnet.form",
        mlnetNav : "local/utils/jquery.mlnet.nav",
        swalModal : "local/utils/swal.init",
        mlnetDatatables : "local/utils/jquery.datatable",
        mlnetScrollto : "local/utils/jquery.mlnet.scrollTo",
        mlnetGoToTop : "local/utils/jquery.mlnet.goToTop",
        mlnetAlert : "local/utils/jquery.mlnet.alert",
        application : "local/application"
        /*Modernizr: 'libs/modernizr/modernizr.min',
        Detectizr: 'libs/detectizr/detectizr',
        waypoint: 'libs/waypoints/jquery.waypoints.min',*/
    },
    shim: {
        conf : {
            exports: 'conf'
        },
        offers : {
            exports: 'offers'
        },
        jquery: {
            deps: [],
            exports: '$',
            init: function () {
                return window.jQuery = $;
            }
        },
        validate: {
            exports: 'validate',
            deps: ['jquery']
        },
        'jquery.validate.addon': {
            exports :'jquery.validate.addon',
            deps: ['validate']
        },
        select2:{
            exports: 'select2',
            deps: ['jquery']
        },
        magnificPopup:{
            exports: 'magnificPopup',
            deps: ['jquery']
        },
        waypoint : {
            exports: 'waypoint',
            deps: ['jquery']
        },
        sticky : {
            exports: 'sticky',
            deps: ['waypoint']
        },
        noUiSlider: {
            exports: 'noUiSlider'
        },
        Chart: {
            exports: 'Chart'
        },
        /*"datatables.net" : {
            exports: 'DataTable',
            deps: ['jquery']
        },*/
        Swal : {
            exports :'Swal'
        },
        prism : {
            exports :'prism'
        },
        i18n : {
            exports: 'i18n',
            deps: ['jquery', 'validate']
        },
        mlnetGoToTop: {
            exports: 'mlnetGoToTop',
            deps:  ['mlnetScrollto']
        },
        application: {
            exports: 'application',
            deps:  ['validate', 'mlnetScrollto','noUiSlider',
                'swalModal',
                'mlnetForm',
                'mlnetNav',
                'mlnetGoToTop',
                'mlnetDatatables',
                'mlnetAlert']
        },
        /*Modernizr: {
            exports: 'Modernizr'
        },
        Detectizr: {
            exports: 'Detectizr'
        },
        waypoint : {
            exports: 'waypoint',
            deps: ['jquery']
        }*/
    },

    out: "../../build/assets/scripts/main.min.js",
    mainConfigFile: 'app.js',
    name: 'main',
    findNestedDependencies: true,
    onBuildRead: function(moduleNames, path, contents) {
        return contents;
        //return contents.replace(/console\.log\(([^\)]+)\);/g, '')
        //              .replace(/debugger;/, '');
    }
})